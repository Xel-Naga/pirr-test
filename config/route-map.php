<?php
return [
    //AUTHORIZATION CONTROLLER
    ['class' => 'yii\rest\UrlRule',
        'controller' => ['auth' => 'auth'],
        'pluralize' => false,
        'except' => ['delete', 'create', 'update', 'view', 'index'],
        'extraPatterns' => [
            'POST login' => 'login',
        ],
    ],

    ['class' => 'yii\rest\UrlRule',
        'controller' => ['rbac' => 'rbac'],
        'pluralize' => false,
        'extraPatterns' => [
            'GET add-role' => 'add-role',
        ],
    ],

    ['class' => 'yii\rest\UrlRule',
        'controller' => ['user' => 'user'],
        'pluralize' => false,
        'extraPatterns' => [
            'POST add-admin' => 'create-admin',
            'POST add-user' => 'create-user',
        ],
    ],

    ['class' => 'yii\rest\UrlRule',
        'controller' => ['event' => 'event'],
        'pluralize' => false,
        'extraPatterns' => [
            'GET get-my-events' => 'get-by-admin',

        ],
    ],
];