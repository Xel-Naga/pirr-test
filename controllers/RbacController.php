<?php

namespace app\controllers;

use app\controllers\common\CommonController;
use app\models\services\auth\AuthItemCrudService;

class RbacController extends CommonController
{
    public function actionAddRole()
    {
        $authItemCrudService = new AuthItemCrudService();
        $authItemCrudService->add();
    }
}