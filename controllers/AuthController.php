<?php

namespace app\controllers;

use app\controllers\common\CommonController;
use app\models\services\auth\AuthService;

class AuthController extends CommonController
{
    public function actionLogin()
    {
     $authService = new AuthService();

     return $authService->authByLogin();
    }
}