<?php

namespace app\controllers;

use app\controllers\common\CommonActiveController;
use app\models\Event;
use app\models\services\event\EventCrudService;
use app\models\services\event\EventLookupService;
use yii\filters\AccessControl;

class EventController extends CommonActiveController
{
    public $modelClass = Event::class;

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = ['options', 'view', 'index'];
        $behaviors['access']['class'] = AccessControl::class;
        $behaviors['access']['rules'] = [
            [
                'actions' => ['index', 'view'],
                'allow' => true,
                'roles' => ['?'],
            ],
            [
                'actions' => ['create', 'update', 'delete', 'get-by-admin'],
                'allow' => true,
                'roles' => ['admin'],
            ],
        ];
        return $behaviors;
    }

    public function actionCreate()
    {
        $eventCrudService = new EventCrudService();

        return $eventCrudService->add();
    }

    public function actionGetByAdmin()
    {
        $eventLookupService = new EventLookupService();

        return $eventLookupService->allByCreatorId();
    }
}