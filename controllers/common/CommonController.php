<?php

namespace app\controllers\common;

use Yii;
use yii\filters\Cors;
use yii\rest\Controller;
use yii\web\Response;

class CommonController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);

        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;


        $behaviors['authenticator'] = $auth;
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }
}