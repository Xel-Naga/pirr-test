<?php

namespace app\controllers;

use app\controllers\common\CommonController;
use app\models\services\user\UserCrudService;

class UserController extends CommonController
{
    public function actionCreateAdmin()
    {
        $userCrudService = new UserCrudService();
        $userCrudService->setRole('admin');

        return $userCrudService->add();
    }

    public function actionCreateUser()
    {
        $userCrudService = new UserCrudService();
        $userCrudService->setRole('user');

        return $userCrudService->add();
    }
}