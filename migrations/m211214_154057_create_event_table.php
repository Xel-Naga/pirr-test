<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%event}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m211214_154057_create_event_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%event}}', [
            'id' => $this->primaryKey(),
            'user_creator_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'description' => $this->text(),
        ]);

        // creates index for column `user_creator_id`
        $this->createIndex(
            '{{%idx-event-user_creator_id}}',
            '{{%event}}',
            'user_creator_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-event-user_creator_id}}',
            '{{%event}}',
            'user_creator_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-event-user_creator_id}}',
            '{{%event}}'
        );

        // drops index for column `user_creator_id`
        $this->dropIndex(
            '{{%idx-event-user_creator_id}}',
            '{{%event}}'
        );

        $this->dropTable('{{%event}}');
    }
}
