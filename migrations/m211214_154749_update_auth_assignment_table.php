<?php

use yii\db\Migration;

/**
 * Class m211214_154749_update_auth_assignment_table
 */
class m211214_154749_update_auth_assignment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('auth_assignment', 'user_id', $this->integer());

        $this->addForeignKey(
            'fk-auth_assignment-user',
            'auth_assignment',
            'user_id',
            'user',
            'id',
            'CASCADE',
        );
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211214_154749_update_auth_assignment_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211214_154749_update_auth_assignment_table cannot be reverted.\n";

        return false;
    }
    */
}
