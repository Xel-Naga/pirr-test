<?php

namespace app\models\forms;

use app\models\User;
use yii\base\Model;

/**
 *
 * @property-read User|null $userByLogin
 */
class AuthForm extends Model
{
    public $login;
    public $email;
    public $password;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            ['login', 'validateLogin'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateLogin($attribute, $params)
    {
        $user = $this->getUserByLogin();
        if (!$this->hasErrors()) {
            if (!isset($user)) {
                $this->addError($attribute, 'LOGIN_DOES_NOT_EXIST');
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        $user = $this->getUserByLogin();
        if (!$this->hasErrors()) {
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'PASSWORD_MATCH');
            }
        }
    }

    /**
     * @return User|null
     */
    public function getUserByLogin()
    {
        return User::findOne(['login' => $this->login]);
    }
}