<?php

namespace app\models\services\user;

use app\models\User;
use Yii;
use yii\base\Exception;

class UserCrudService
{
    private string $role;

    /**
     * @param string $role
     * @return UserCrudService
     */
    public function setRole(string $role): UserCrudService
    {
        $this->role = $role;
        return $this;
    }

    public function add()
    {
        $user = new User();
        $auth = Yii::$app->authManager;
        $transaction = Yii::$app->getDb()->beginTransaction();

        try {
            if ($user->load(Yii::$app->request->bodyParams, '') && $user->validate()) {

                $user->password = Yii::$app->getSecurity()->generatePasswordHash($user->password);
                $user->save();

                $auth->assign($auth->getRole($this->role), $user->getPrimaryKey());
                $transaction->commit();
                return $user;
            }
        } catch (Exception | \Exception $e) {
            $transaction->rollBack();
            return $e->getTrace();
        }
        return $user;
    }
}