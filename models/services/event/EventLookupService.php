<?php

namespace app\models\services\event;

use app\models\Event;
use Yii;

class EventLookupService
{
    public function allByCreatorId(): array
    {
        return Event::findAll(['user_creator_id' => Yii::$app->user->id]);
    }
}