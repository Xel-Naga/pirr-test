<?php

namespace app\models\services\event;

use app\models\Event;
use Yii;

class EventCrudService
{
    public function add()
    {
        $event = new Event();
        $event->user_creator_id = Yii::$app->user->id;
        $transaction = Yii::$app->getDb()->beginTransaction();

        try {
            if ($event->load(Yii::$app->request->bodyParams, '') && $event->validate()) {

                $event->save();
                $transaction->commit();
                return $event;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            return $e->getTrace();
        }
        return $event;
    }
}