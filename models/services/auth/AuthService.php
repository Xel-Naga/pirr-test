<?php

namespace app\models\services\auth;

use app\models\forms\AuthForm;
use app\models\User;
use app\models\UserToken;
use Yii;
use yii\db\Exception;

class AuthService
{
    public function authByLogin()
    {
        $authForm = new AuthForm();

        if ($authForm->load(Yii::$app->request->bodyParams, '') && $authForm->validate()) {

            return ['token' => $this->getAccessToken($authForm->login)];
        }
        Yii::$app->response->statusCode = 422;
        return ['error' => $authForm->getErrors()];
    }

    /**
     * @param $login
     * @return false[]|string
     */
    private function getAccessToken($login)
    {
        $user = User::find()->where(['login' => $login])->one();
        $userToken = $user->userTokens[0] ?? null;

        if (isset($userAccessTokens)) {
            return $userToken->token;
        }

        return $this->createAccessToken($user->id);
    }

    private function createAccessToken($userId)
    {
        $userToken = new UserToken();
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $userToken->user_id = $userId;
            $userToken->token = Yii::$app->security->generateRandomString(255);
            $userToken->save();

            $transaction->commit();
            return $userToken->token;

        } catch (Exception $e) {
            $transaction->rollBack();
            return $e->getTrace();
        } catch (\yii\base\Exception $e) {
            $transaction->rollBack();
            return $e->getTrace();
        }
    }
}