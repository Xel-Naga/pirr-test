<?php

namespace app\models\services\auth;

use Yii;

class AuthItemCrudService
{
    public function add()
    {
        $auth = Yii::$app->authManager;

        $user = $auth->createRole('user');
        $auth->add($user);

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $user);
    }
}